**Scenario**

*Add scenario of use case*

Example:
1. A user wants to configure the system.
2. The user changes the configuration.
3. The user restart the system.
4. The system operates with the new configuration.

/label ~"Use Case"
